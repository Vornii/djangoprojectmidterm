import datetime

from django.shortcuts import render

# Create your views here.

from django.shortcuts import render
from django.http import HttpResponse
from django.template import loader

from app1.forms import DepartmentForm, PositionForm

#controller where we write logic code before return response to browser
def index(request):
    today = datetime.datetime.now().date()
    pros = ['SS2','SS3','SS4','SS5']
    return render(request,"hometem/app.html",{
        "today":today,
        "codes":pros
    })
def appa(request):
    return HttpResponse("<center>Hello Django  from app 2</center>")

def appb(request):
    print(request)

    return HttpResponse("<h2>Hello Django from app 3</h2>")





def home(request):
    return render(request,'hometem/home.html')



def data_dept(request):
    if request.method == 'POST':
        form = DepartmentForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponse('<p>Info Saved!</p>')
        else:
            return HttpResponse('<p>Info is not Valid</p>')
    else:
        form = DepartmentForm
        context = {
            'form': form,
        }
        return render(request, 'app/department.html', context)

def data_posit(request):
    if request.method == 'POST':
        form =  PositionForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponse('<p>Info Saved!</p>')
        else:
            return HttpResponse('<p>Info is not Valid</p>')
    else:
        form =  PositionForm
        context = {
            'form': form,
        }
        return render(request, 'app/department.html', context)

