from django import forms
from django.forms import ModelForm
from .models import Department
from .models import Position

class DepartmentForm(ModelForm):

    class Meta:
        model = Department
        fields = ['name','created_at']

class PositionForm(ModelForm):
    class Meta:
        model = Position
        fields = ['name','created_at']
