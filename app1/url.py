

from django.contrib import admin
from django.urls import path
from . import views

urlpatterns = [
    # create url path for each route to link the view function
    path("", views.index, name="index"), #localhost:8000/app
    path("home",views.home),
    path("appa",views.appa,name="index"),
    path("appb",views.appb,name="index"),
    path("department",views.data_dept),
        path("position",views.data_posit)
]