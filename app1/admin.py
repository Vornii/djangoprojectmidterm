from django.contrib import admin
from .models import *

# Register your models here.
class DepartmentModel(admin.ModelAdmin):
    list_display=["name","created_at"]
class PositionModel(admin.ModelAdmin):
    list_display=["name","created_at"]

class PeopleModel(admin.ModelAdmin):
    list_display  = ["code","DepId","PosId"]
admin.site.register(Department,DepartmentModel)
admin.site.register(Position,PositionModel)
admin.site.register(People)

