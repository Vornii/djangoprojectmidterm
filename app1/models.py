from django.db import models
from datetime import datetime
# Create your models here.


class Department(models.Model):
    name = models.CharField(max_length=100)
    created_at = models.DateTimeField(default=datetime.now,blank=True)

    def __str__(self):
        return self.name
    

class Position(models.Model):
    name = models.CharField(max_length=250)
    created_at = models.DateTimeField(default=datetime.now,blank=True)

class People(models.Model):
    ["code","DepID","PTID","firstname","lastname","dob","create_at"]
    firstname = models.CharField(max_length=100,default="Siv")
    lastname = models.CharField(max_length=100,default="Panha")
    dob = models.DateField(max_length=8,default=datetime.now)
    DepId = models.ForeignKey(Department,on_delete=models.CASCADE)
    PosId = models.ForeignKey(Position,on_delete=models.CASCADE)
    code  = models.CharField(max_length=150)
