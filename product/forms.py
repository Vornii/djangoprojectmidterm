from django import forms


class MyForm (forms.Form):
    # Define your fields and validators
    firstname = forms.CharField (
        label="Firstname",

        error_messages= {
            "required":"You name must not be empty",
            "max_length":"Please enter shorter"

        },
        required=False,
        max_length=50,min_length= 3
        
        )
    lastname = forms.CharField (
        label="Lastname",

        error_messages= {
            "required":"You name must not be empty",
            "max_length":"Please enter shorter"

        },
        required=False,
        max_length=50,min_length= 3
        
        )
    telephone = forms.CharField (
        label="Telephone",

        error_messages= {
            "required":"You name must not be empty",
            "max_length":"Please enter shorter"

        },
        required=False,
        max_length=50,min_length= 3
        
        )
    
    address = forms.CharField (

        label="Home Address",

        error_messages= {
            "required":"You address must not be empty",
            "max_length":"Please enter shorter"

        },
        required=False,
        max_length=250,min_length= 5
        
        )
