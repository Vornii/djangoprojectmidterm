from django.contrib import admin

# Register your models here.
from django.contrib import admin
from .models import *

class CategoriesAdmin(admin.ModelAdmin):
    list_display = ["name","des", "create_at"]

class StockAdmin(admin.ModelAdmin):
    list_display = ["name","des", "create_at"]

class PTypeAdmin(admin.ModelAdmin):
    list_display = ["name", "des", "create_at"]

class ImagesAdmin(admin.ModelAdmin):
    list_display = ["name","images", "create_at"]

class PriceAdmin(admin.ModelAdmin):
    list_display = ["name","price", "create_at"]

class BrankAdmin(admin.ModelAdmin):
    list_display = ["name","des", "create_at"]


class TagAdmin(admin.ModelAdmin):
    list_display = ["name","des", "create_at"]
class AttributetAdmin(admin.ModelAdmin):
    list_display = ["name","des", "create_at"]
class ProductAdmin(admin.ModelAdmin):
    list_display  = ["code","CATID","STID","name","qty",'des',"create_at"]



admin.site.register(Categories, CategoriesAdmin)
admin.site.register(Stock, StockAdmin)
admin.site.register(Images,ImagesAdmin)
admin.site.register(Branch,BrankAdmin)
admin.site.register(Tag,TagAdmin)
admin.site.register(Price,PriceAdmin)
admin.site.register(Attribution,AttributetAdmin)
admin.site.register(Products,ProductAdmin)

# Register your models here.