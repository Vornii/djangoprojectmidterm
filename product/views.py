from datetime import date
from typing import Any
from django.db import models
from django.http import HttpRequest
from django.shortcuts import redirect, render
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic import TemplateView

from product.forms import MyForm
from .models import *
# Create your views here.
import requests

class ProductsDetail(DetailView):
   template_name = "app/productdetail.html"
   model = Products

   
   



class ProductAllDetail(DetailView):
   model = Products
   context_object_name = "products"
   template_name = "app/pdetail.html"
   def get_context_data(self, **kwargs):
       context = super().get_context_data(**kwargs)
    
       context["image"] = Images.objects.all()
       return context

class CategoryAllDetail(DetailView):
   model = Categories
   context_object_name = "categories"
   template_name = "app/catedetail.html"




class ProductListView(ListView):
 model = Products
#  model = Categories

 template_name = "app/product.html"
 context_object_name = "products"
 def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)


        if(self.request.GET.get("gid")):
         context['categories'] = Categories.objects.all()
         context['products']  = Products.objects.filter( CATID = self.request.GET.get("gid"))
        else:
         context['categories'] = Categories.objects.all()
        return context

class CategoryListView(ListView):
 model = Products
 model = Categories
 template_name = "app/categories.html"
 context_object_name = "categories"

class ProductDetail(DetailView):
 model = Products 
template_name = "app/category/categories.html"


class OrderDetail(TemplateView):
   template_name = "app/orderdetail.html"



def send_msg(request: HttpRequest):
    token = "6387214551:AAGwSXl5auoFMgegymsQMjBIqNF5LgnexRo"
    #telegram group link
    chat_id = "@djangosale"
    form =  MyForm()
    print(request.POST)
    if request.method == 'POST':
     form = MyForm(request.POST)
     if form.is_valid():

      productdetail = Products.objects.get(pk=request.GET.get('id'))
 
      ordermsg = f"""
      
      Your Order has been placed
      ----------------------------------------
      User Information
      ----------------------------------------     
      Username: {form.cleaned_data['firstname']} {form.cleaned_data['lastname']}  
      Telephone: {form.cleaned_data['telephone']}    
      Address Deliver: {form.cleaned_data['address']}  
      ----------------------------------------
      Order Date: {date.today()}    
      Product Item: {productdetail.name}    
      Product Price: $ {productdetail.PRIID.price}     
      Product Qty: {request.POST['qtyproduct']}  
      ----------------------------------------
      
     """

      url_req = "https://api.telegram.org/bot" + token + "/sendMessage" + "?chat_id=" + chat_id + "&text=" +  ordermsg 
      results = requests.get(url_req)
      return redirect("/orderdetail/success")
     else:
          product = Products.objects.filter(id=request.GET.get('id')).all()
 
    
          categories= Categories.objects.all().order_by('parent')# added all
          return render(request,
                  "app/product/product_detail.html",
          

                  {"product": product,
                           "form" :   form ,
                   "categories": categories, })
    else :
     return redirect("/orderdetail/success")

  

  






def list_products(request):
    #products= Products.objects.all()  # added all
     categories= Categories.objects.all().order_by('parent') # added all

     active = -1
     link = True
     if request.GET.get('gid') is None :
      products=Products.objects.all()
     else:
      active = request.GET.get('gid')
      products=Products.objects.filter(CATID=request.GET.get('gid')).all()
  


     return render(request,
                  "app/product/allproduct.html",
                  {"products": products,
                   "item":link,
                   "active":active,
                   "categories": categories,})
def list_productsall(request):
     categories= Categories.objects.all().order_by('parent') # added all
     link = True
     active = -1
     if request.GET.get('gid') is None :
      products=Products.objects.all()
     else:
      active = request.GET.get('gid')
      products=Products.objects.filter(CATID=request.GET.get('gid')).all()
  


     return render(request,
                  "app/product/allproduct.html",
                  {"products": products,
                   "active":active,
                         "item":link,
                   "categories": categories,})

def list_catectories(request):
    categories= Categories.objects.all()  # added all
    products = Products.objects.all()  # added all
    return render(request,
                  "app/product/categories.html",
                  {"categories": categories,"products": products})


def list_products(request):
    #products= Products.objects.all()  # added all
    categories= Categories.objects.all().order_by('parent') # added all

    active = -1
    if request.GET.get('gid') is None :
     products=Products.objects.all()
    else:
     active = request.GET.get('gid')
     products=Products.objects.filter(CATID=request.GET.get('gid')).all()
  


    return render(request,
                  "app/product/product.html",
                  {"products": products,
                   "active":active,
                   "categories": categories,})

def list_catectories(request):
    categories= Categories.objects.all().order_by('parent')  # added all
    products = Products.objects.all()  # added all
    return render(request,
                  "app/product/categories.html",
                  {"categories": categories,
                   "products": products})

def prodetail(request):
    product = Products.objects.filter(id=request.GET.get('id')).all()
    form =  MyForm()
    
    categories= Categories.objects.all().order_by('parent')# added all
    return render(request,
                  "app/product/product_detail.html",
          

                  {"product": product,
                           "form" :   form ,
                   "categories": categories, })