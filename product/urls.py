

from django.contrib import admin
from django.urls import path
from . import views

# urlpatterns = [

#         path("all", views.ProductListView.as_view()),
#              path("category", views.CategoryListView.as_view()),
#              path("all/<int:pk>",views.ProductAllDetail.as_view()),


#              path('listpro', views.ProductListView.as_view(), name='listpro'),

#              path('listpro/<int:pk>',views.ProductsDetail.as_view())

#     #    path("listpro", views.listproduct)

# ]

urlpatterns = [
    #path("", views.index, name="index"),
    path('', views.list_products,name="Product View"),
        path('listpro/product', views.list_productsall,name="Product View2"),
    path("listcat", views.list_catectories,name="Catecgory View"),
    path("listpro/prodetail", views.prodetail,name="Product Detail"),
   path("orderdetail/success", views.OrderDetail.as_view(),name="Order"),
   path("orderbot",views.send_msg,name="Bot Send Message")

]