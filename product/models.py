from django.db import models
from datetime import datetime

class Categories(models.Model):
    CATID = models.ForeignKey('self', on_delete=models.CASCADE,null=True,blank=True)
    name = models.CharField(max_length=100)
    image = models.ImageField(upload_to="static/img/uploads/", max_length=100, default=None, blank=True, null=True)
    code = models.CharField(max_length=255, null=True)
    parent = models.BooleanField(null=True)
    des = models.TextField(max_length=1000, null=True)
    create_at = models.DateTimeField(default=datetime.now, blank=True)

    def __str__(self):
        return str(self.name)

class Tag(models.Model):
    name = models.CharField(max_length=100,null=True)
    image = models.ImageField(upload_to="static/img/uploads/", max_length=100, default=None, blank=True, null=True)
    des = models.TextField(max_length=1000, null=True)
    create_at = models.DateTimeField(default=datetime.now, blank=True)

    def __str__(self):
        return self.name

class Branch(models.Model):
    name = models.CharField(max_length=100,null=True)
    name = models.CharField(max_length=100, null=True)
    code = models.CharField(max_length=255, null=True)
    image = models.ImageField(upload_to="static/img/uploads/", max_length=100, default=None, blank=True, null=True)
    des = models.TextField(max_length=1000, null=True)
    create_at = models.DateTimeField(default=datetime.now, blank=True)

    def __str__(self):
        return str(self.name)+"-"+str(self.code)

class Stock(models.Model):
    name = models.CharField(max_length=100, null=True)
    code = models.CharField(max_length=255, null=True)
    image = models.ImageField(upload_to="static/img/uploads/", max_length=100, default=None, blank=True, null=True)
    des = models.TextField(max_length=1000, null=True)
    create_at = models.DateTimeField(default=datetime.now, blank=True)

    def __str__(self):
        return str(self.name)+"-"+str(self.code)

class Price(models.Model):
    name = models.CharField(max_length=100,null=True)
    code = models.CharField(max_length=255,null=True)
    price = models.FloatField(max_length=100)
    des = models.TextField(max_length=1000, null=True)
    create_at = models.DateTimeField(default=datetime.now, blank=True)

    def __str__(self):
        return str(self.name)+"-"+str(self.code)

class Attribution(models.Model):
    name = models.CharField(max_length=100,null=True)
    image = models.ImageField(upload_to="static/img/uploads/",  max_length=100,default=None, blank=True, null=True)
    des = models.TextField(max_length=1000, null=True)
    create_at = models.DateTimeField(default=datetime.now, blank=True)

    def __str__(self):
        return self.name

class Images(models.Model):
    name = models.CharField(max_length=100,null=True)
    images = models.ImageField(upload_to="static/img/uploads/",  max_length=100, default=None, blank=True, null=True)
    des = models.TextField(max_length=1000, null=True)
    create_at = models.DateTimeField(default=datetime.now)
    def __str__(self):
        return self.name

class Products(models.Model):
    CATID = models.ForeignKey(Categories, on_delete=models.CASCADE)
    STID = models.ForeignKey(Stock, on_delete=models.CASCADE)
    ATTID = models.ManyToManyField(Attribution)
    PRIID = models.ForeignKey(Price, on_delete=models.CASCADE,null=True)
    BRAID = models.ForeignKey(Branch, on_delete=models.CASCADE,null=True)
    code = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    IMGID = models.ManyToManyField(Images)
    qty = models.IntegerField(max_length=100, null=True)
    des = models.TextField(max_length=1000, null=True)
    create_at = models.DateTimeField(default=datetime.now, blank=True)

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['code'], name='unique product_code')
        ]

    def __str__(self):
        return self.code