from django.shortcuts import render
from django.http import HttpRequest,HttpResponse,HttpResponseServerError,Http404
# Create your views here.


challenges = {
    "January": "your description Django Jan",
    "February": "your description Django feb",
    "March": "your description Django March",
    "April": "your description Django Apri",
    "May": "your description Django May",
    "June": "your description Django June",
    "July": "your description Django July",
    "August": "your description Django Aug",
    "September": "your description Django Sep",
    "October": "your description Django Oct",
    "November": "your description Django Nov",
    "December": "your description Django Dec"
}


def home(request):
    try:

     return render(request,'challenge/all.html',{
        "month":challenges
     })
    except:
     return HttpResponseServerError("<h1>Not Found</h1>")

def index(request,home):
    segment = home
    # jan , month 
    try:

     return render(request,'challenge/main.html',{
        "month":challenges[segment]
    })
    except:
     raise Http404()


